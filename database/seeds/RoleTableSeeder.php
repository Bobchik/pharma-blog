<?php

use Illuminate\Database\Seeder;

class RoleTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $role = new \App\Role([
            'name' => 'admin',
            'description' => 'admin'
        ]);
        $role->save();

        $role = new \App\Role([
            'name' => 'user',
            'description' => 'user'
        ]);
        $role->save();

        $role = new \App\Role([
            'name' => 'editor',
            'description' => 'editor'
        ]);
        $role->save();
    }
}
