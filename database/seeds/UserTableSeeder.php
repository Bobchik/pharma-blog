<?php

use Illuminate\Database\Seeder;

class UserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $user = new \App\User([
            'name' => 'Admin',
            'email' => 'admin@admin.com',
            'password' => bcrypt('123456')
        ]);
        $user->save();

        $user = new \App\User([
            'name' => 'User',
            'email' => 'test@test.com',
            'password' => bcrypt('123456')
        ]);

        $user->save();
        $user = new \App\User([
            'name' => 'Editor',
            'email' => 'test1@test.com',
            'password' => bcrypt('123456')
        ]);
        $user->save();
    }
}
