$(function() {
    $('body').on('click', '.pagination a', function(e) {
        e.preventDefault();

        $('#load a').css('color', '#dfecf6');

        var url = $(this).attr('href');
        getPosts(url);
        window.history.pushState("", "", url);
    });

    function getPosts(url) {
        $.ajax({
            url : url
        }).done(function (data) {
            $('body').html(data);
        }).fail(function () {
            alert('Articles could not be loaded.');
        });
    }
});