<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Post;
use Illuminate\Support\Facades\Storage;

class PostController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth')->except(['index', 'show']);
    }

    public function index()
    {
        $posts = Post::latest()
//            ->filter(request(['month', 'year']))
            ->paginate(5);

        if (request()->ajax()){
            return view('posts.index', compact('posts'))->render();
        }

        return view('posts.index', compact('posts'));
    }

    public function show(Post $post)
    {
        return view('posts.show', compact('post'));
    }

    public function create()
    {
        return view('posts.create');
    }

    public function store(Request $request)
    {
        $this->validate(request(), [
            'title' => 'required|string',
            'alias' => 'required|string|min:3',
            'content' => 'required|string',
        ]);

        $store = request()->file('img')->store('public/images');
        $path = substr($store, 7);

        auth()->user()->publish(new Post([
            'title' => $request->input('title'),
            'alias' => $request->input('alias'),
            'content' => $request->input('content'),
            'img' => 'storage/'.$path
        ]));

        return redirect()->route('home');
    }

    public function edit(Post $post)
    {
        return view('posts.edit');
    }

    public function delete(Post $post)
    {
        $post->delete();
        return redirect()->route('home');
    }

}
