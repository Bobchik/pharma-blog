<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;

class Post extends Model
{
    protected $fillable = [
        'title', 'alias', 'content', 'img', 'created_at', 'author_id'
    ];

    public function getRouteKeyName()
    {
        return 'alias';
    }

    public function author()
    {
        return $this->belongsTo(User::class, 'author_id');
    }

    public function comments()
    {
        return $this->hasMany(Comment::class);
    }

    public function addComment($body)
    {
        $this->comments()->create(compact('body'));
    }

    public function scopeFilter($query, $filters)
    {
        if($month = $filters['month']){

            $query->whereMonth('created_at', Carbon::parse($month)->month);
        }

        if($year = $filters['year']){

            $query->whereYear('created_at', $year);
        }
    }

    public static function archives()
    {
        return static::selectRaw('year(created_at) year, monthname(created_at) month, count(*) published')
            ->groupBy('year', 'month')
            ->orderByRaw('min(created_at) desc')
            ->get()
            ->toArray();
    }

    public static function recent()
    {
        return static::selectRaw('title, alias, count(*) published')
            ->groupBy('title', 'alias')
            ->orderByRaw('min(created_at) desc')
            ->limit(5)
            ->get()
            ->toArray();
    }
}
