<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//Route::get('/', function () {
//    return view('welcome');
//});

Auth::routes();


Route::get('/', 'PostController@index')->name('home');
Route::get('/posts/create', 'PostController@create');
Route::get('/posts/{post}', 'PostController@show');
Route::post('/posts', 'PostController@store');
Route::get('/posts/{post}/edit', 'PostController@edit');
Route::delete('/posts/{post}', 'PostController@delete');

Route::get('/admin', function (){
    return view('auth.login');
});
Route::get('/home', 'HomeController@admin')->name('admin-home');
Route::get('/about', function (){
    return view('layouts.about');
});
Route::get('/contact', function (){
    return view('layouts.contact');
});

Route::post('/posts/{post}/comments', 'CommentController@store');
