@extends('welcome')

@section('content')
    <div class="single">
        <div class="container animated lightSpeedIn">
            <div class="col-md-8 single-main">
                <div class="single-grid">
                    <h2>{{$post->title}}</h2>
                    <p>{{$post->created_at}} - by: {{$post->author->name}}</p>
                    <img src="{{asset($post->img)}}" alt="">
                    <p>{{$post->content}}</p>
                    @if(Auth::id() == $post->author_id || Auth::id() == 1)
                        <form action="{{$post->alias}}" method="post">
                            {{csrf_field()}}
                            {{method_field('DELETE')}}
                            <input type="hidden" name="id" value="{{$post->alias}}">
                            <button type="submit" class="btn btn-danger">Delete post</button>
                        </form>
                    @endif
                    <h2>Comments:</h2>
                    <ul class="list-group">
                        @foreach($post->comments as $comment)
                            <li class="list-group-item">
                                <strong>
                                    <p>{{ $comment->user_name }} {{ $comment->created_at }} </p>
                                </strong>
                                {{ $comment->body }}
                            </li>
                        @endforeach
                    </ul>
                    <hr>

                    <div class="col-md-10">
                        <div class="card">
                            <div class="card-block">
                                <form action="/posts/{{ $post->alias }}/comments" method="POST">
                                    {{csrf_field()}}

                                    <label for="user_name">Your name</label>
                                    <div class="form-group">
                                        <input type="text" class="form-control" name="user_name">
                                    </div>

                                    <div class="form-group">
                                            <textarea name="body" placeholder="Your comment here"
                                                      class="form-control"></textarea>
                                    </div>

                                    <div class="form-group">
                                        <button class="btn btn-primary" type="submit">Add Comment</button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection