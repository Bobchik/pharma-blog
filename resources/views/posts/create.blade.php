@extends('welcome')

@section('content')
    <div class="col-md-6">
        <form method="post" action="/posts" enctype="multipart/form-data">
            <div class="form-group">
                {{ csrf_field()}}
                <label for="title">Title</label>
                <input type="text" name="title" class="form-control" id="title" placeholder="Title">
            </div>
            <div class="form-group">
                <label for="alias">Alias</label>
                <input type="text" name="alias" class="form-control" id="alias" placeholder="Alias">
            </div>

            <div class="form-group">
                <input type="file" name="img" class="form-control" id="img">
            </div>
            <div class="form-group">
                <label for="content">Article</label>
                <textarea name="content" class="form-control" id="content" cols="30" rows="10"
                          placeholder="Article"></textarea>
            </div>
            <div class="form-group">
                <button type="submit" class="btn btn-primary">Publish</button>
            </div>
        </form>
        {{--        @include('errors.errors')--}}
    </div>

@endsection