@extends('welcome')

@section('content')
    <div class="col-md-8 content-main animated bounce">
        @foreach($posts as $post)
            <div class="content-grid">
                <div class="content-grid-info">
                    <img src="{{asset($post->img)}}" alt="" width="600px"/>
                    <div class="post-info">
                        <h4>
                            <a href="/posts/{{ $post->alias }}">{{$post->title}}</a> {{$post->created_at->diffForHumans()}}
                            by {{$post->author->name}}</h4>
                        <p>{{str_limit($post->content, 400)}}</p>
                        <a href="/posts/{{ $post->alias }}"><span></span>Read more</a>
                    </div>
                </div>
            </div>
        @endforeach
    </div>
    <div class="col-md-4 content-right animated bounceInRight">
        <div class="archives">
            <div class="sidebar-module">
                <h3>ARCHIVES</h3>
                <ol class="list-unstyled">
                    @foreach($archives as $archive)
                        <li><a href="/?month={{$archive['month']}}&year={{$archive['year']}}">
                                {{$archive['month'] .' '. $archive['year']}}
                            </a>
                        </li>
                    @endforeach
                </ol>
            </div>
        </div>
    </div>

    <div class="col-md-4 content-right animated bounceInRight">
        <div class="recent">
            <h3>RECENT POSTS</h3>
            <ul>
                @foreach($recent as $value)
                <li><a href="/posts/{{$value['alias']}}">{{$value['title']}}</a></li>
                    @endforeach
            </ul>
        </div>
    </div>
@endsection